import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

@NgModule({
  imports: [CommonModule],
  exports: [HttpClientTestingModule, ReactiveFormsModule, FormsModule],
})
export class TestingModule { }
