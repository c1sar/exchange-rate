import { Component, OnInit, AfterViewInit, ElementRef, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { ExchangeService } from './exchange.service';
import { CurrencyConst } from './currency.util';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, AfterViewInit {

  @ViewChild('inputAmount') inputAmount: ElementRef;

  rates: Object;
  form: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private exchangeService: ExchangeService) { }

  ngOnInit() {
    this.form = this.formBuilder.group({
      amountToChange: ['', [Validators.required, Validators.maxLength(7)]],
      amountChanged: ['', []]
    });

    this.form.get('amountChanged').disable();
    this.exchangeService.getExchangeRates().subscribe(data => this.rates = data.rates);
  }

  ngAfterViewInit() {
    this.inputAmount.nativeElement.focus();
  }

  calculateExchangeRate(event: Event): void {
    event.preventDefault();
    const amount = parseFloat(this.form.get('amountToChange').value);
    this.form.get('amountChanged').patchValue(this.calculateAmount(amount));
  }

  private calculateAmount(amount: number, symbol: string = CurrencyConst.defaultExchange): number {
    return this.rates[symbol] * amount;
  }
}
