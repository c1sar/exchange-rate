export interface IExchangeRateModel {
  base: string;
  date: string;
  rates: Object;
}
