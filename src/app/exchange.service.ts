import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable, BehaviorSubject } from 'rxjs';
import { tap, switchMap } from 'rxjs/operators';
import { timer } from 'rxjs';

import { IExchangeRateModel } from './models/exchange-rate.model';
import { CurrencyConst } from './currency.util';

@Injectable({
  providedIn: 'root'
})
export class ExchangeService {

  private _rates: BehaviorSubject<Object> = new BehaviorSubject(<Object>null);

  constructor(private http: HttpClient) { }

  get rates(): Object {
    return this._rates.getValue();
  }

  getExchangeRates(currency: string = CurrencyConst.default, symbols?: string | string[]): Observable<IExchangeRateModel> {
    let symbolsParam: string | string[] = CurrencyConst.defaultExchange;

    if (symbols) {
      symbolsParam = (typeof symbols) === 'string' ? symbols : (symbols as string[])
        .reduce((oldElement, newElement) => `${oldElement},${newElement}`);
    }

    const url = `${environment.apiUrl}?access_key=${environment.accessKey}&base=${currency}&symbols=${symbolsParam}`;

    return timer(0, 600000).pipe(
      switchMap(() => this.http.get<IExchangeRateModel>(url)),
      tap(data => this._rates.next(data.rates))
    );
  }

}
