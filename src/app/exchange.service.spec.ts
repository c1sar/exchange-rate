import { HttpTestingController } from '@angular/common/http/testing';
import { getTestBed, inject, TestBed } from '@angular/core/testing';

import { TestingModule } from './testing.module';
import { ExchangeService } from './exchange.service';

describe('ExchangeService', () => {
  let httpMock: HttpTestingController;
  let service: ExchangeService;
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [TestingModule],
      providers: [ExchangeService]
    });

    const injector = getTestBed();
    service = injector.get(ExchangeService);
    httpMock = injector.get(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

});
