import { of, Observable } from 'rxjs';

import { IExchangeRateModel } from './models/exchange-rate.model';

export const ratesMock: IExchangeRateModel = {
  base: 'USD',
  date: '2018-11-23',
  rates: { EUR: 0.876395 }
};

export class ExchangeServiceStub {

  getExchangeRates(): Observable<IExchangeRateModel> {
    return of(ratesMock);
  }

  get rates(): IExchangeRateModel {
    return ratesMock;
  }
}
