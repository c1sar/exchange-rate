export class CurrencyConst {
  static default: string = 'USD';
  static defaultExchange: string = 'EUR';
}
