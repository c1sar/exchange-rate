import {AfterViewChecked, Directive, ElementRef, HostListener, Inject, Input, LOCALE_ID} from '@angular/core';

@Directive({
  selector: '[appFilterAmount]'
})
export class FilterAmountDirective implements AfterViewChecked {

  @Input()
  maxLength: number;

  @Input()
  step: number = 0.01;

  inputSelected: boolean = false;

  constructor(private el: ElementRef, @Inject(LOCALE_ID) private _locale: string) {
  }

  get element(): HTMLInputElement {
    return this.el.nativeElement;
  }

  get value(): string {
    return this.element.value;
  }

  get isFocused(): boolean {
    return document && this.el.nativeElement === document.activeElement;
  }

  set value(newValue: string) {
    this.element.value = newValue;
  }

  ngAfterViewChecked() {
    if (!this.isFocused) {
      this.addDecimalDigits();
    }
  }

  addDecimalDigits() {
    if (this.value === '') {
      return;
    }
    const value = Number(this.value); // this.element.valueAsNumber is fails on IE11
    const decimalDigits = (this.step.toString().split('.')[1] || []).length;
    if (Number.isFinite(value) && (!this.maxLength || this.value.length <= (this.maxLength - decimalDigits - 1))) {
      this.value = value.toFixed(decimalDigits);
    }
  }

  @HostListener('keypress', ['$event'])
  onKeyPress(event: KeyboardEvent): void {
    if (!event || !event.charCode) {
      return;
    }

    const addedValue = String.fromCharCode(event.charCode);

    this.filterAmount(event, addedValue);
  }

  @HostListener('select', ['$event'])
  onSelect(event: any): void {
    if (!event || !this.value) {
      this.inputSelected = false;
      return;
    }
    this.inputSelected = true;
  }

  @HostListener('paste', ['$event'])
  onPaste(event: any): void {
    const clipboardData: any = (<any>window)['clipboardData'];
    if (!(event && event.clipboardData) && !clipboardData) {
      event.preventDefault();
      return;
    }

    let pastedValue: string;
    if (event.clipboardData && event.clipboardData.getData) {
      pastedValue = event.clipboardData.getData('text/plain');
    } else if (clipboardData && clipboardData['getData']) { // IE
      pastedValue = clipboardData.getData('Text');
    }

    this.filterAmount(event, pastedValue);
  }

  @HostListener('drop', ['$event'])
  onDrop(event: any): void {
    if (!event || !event.dataTransfer) {
      event.preventDefault();
      return;
    }

    const pastedValue: string = event.dataTransfer.getData('text');

    if (!pastedValue) {
      event.preventDefault();
      return;
    }

    this.filterAmount(event, pastedValue);
  }

  /**
   * Will prevent entering more than 2 digits
   */
  @HostListener('input')
  onInput() {
    const valueNumber: number = Number(this.value); // this.element.valueAsNumber is fails on IE11
    if (!Number.isFinite(valueNumber) || (this.maxLength && this.value.length > this.maxLength)) {
      this.value = this.value.slice(0, this.maxLength);
    } else {
      const decSeparator = this.getDecimalSeparator(this.value);
      if (Number.isFinite(valueNumber) && this.value.includes(decSeparator)) {
        const decimals = this.value.split(decSeparator)[1].length;
        if (decimals > 2) {
          this.value = `${this.value.split(decSeparator)[0]}.${this.value.split(decSeparator)[1].substring(0, 2)}`;
        }
      }
    }

  }

  @HostListener('change')
  onChange() {
    this.addDecimalDigits();
  }

  private filterAmount(event: Event, addedValue: string): void {
    const newValue = '' + this.value + addedValue;
    const newNumber = Number(newValue);

    if (!this.el.nativeElement.checkValidity() || !Number.isFinite(newNumber)) {
      event.preventDefault();
    }

  }

  private getDecimalSeparator(amount: string) {
    const decSeparator = '.';
    if (amount.includes(decSeparator) || amount.includes('..')) {
      return decSeparator;
    }
    return ',';
  }
}
